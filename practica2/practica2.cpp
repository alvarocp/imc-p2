//============================================================================
// Introducción a los Modelos Computacionales
// Name        : practica1.cpp
// Author      : Pedro A. Gutiérrez
// Version     :
// Copyright   : Universidad de Córdoba
//============================================================================


#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <iostream>
#include <ctime>    // Para cojer la hora time()
#include <cstdlib>  // Para establecer la semilla srand() y generar números aleatorios rand()
#include <string.h>
#include <math.h>
#include "imc/PerceptronMulticapa.h"

using namespace imc;
using namespace std;

int main(int argc, char **argv) {
	// Procesar los argumentos de la línea de comandos

	// Objeto perceptrón multicapa
	PerceptronMulticapa mlp;

	// Parámetros del mlp. Por ejemplo, mlp.dEta = valorQueSea;

	char *train = NULL , * Test = NULL;
	int iteraciones = 1000, hiddenLayers = 1, hvalue = 5,fError = 0;
	float eta = 0.7, mu = 1, validationRatio = 0.0, F = 1;
	bool s = false, online = false;

	int opcion;


	while( (opcion = getopt(argc, argv, "t:T:i:l:h:e:m:v:d:sof:")) != -1)
		{
				switch(opcion)
				{
					case 't': //Fichero de train
					train = optarg;
					break;
					case 'T': //Fichero de test
						Test = optarg;
						break;
					case 'i': //Número de iteraciones del bucle externo
						iteraciones = atoi(optarg);
						break;
					case 'l': //Número de capas ocultas
						hiddenLayers = atoi(optarg);
						break;
					case 'h': //Número de neuronas en capa oculta
						hvalue = atoi(optarg);
						break;
					case 'e': //Coeficiente de aprendizaje (eta)
						eta = atof(optarg);
						break;
					case 'm': //Momento (mu)
						mu = atof(optarg);
						break;
					case 'v': //Ratio patrones validacion
						validationRatio = atof(optarg);
						break;
					case 'd': //Factor de decremento
						F = atof(optarg);
						break;
					case 's':
						s = true;
						break;
					case 'o':
						online = true;
						break;
					case 'f':
						fError = atoi(optarg);
				}
		}

	//cout << "Parámetros de entrada " << train << Test << iteraciones << hiddenLayers << hvalue << eta << mu <<validationRatio << F << "softmax: " << s << "Online: " << online <<  "Funcion de error: " << fError << std::endl;

	if(train == NULL)
	{
		cerr << "Falta fichero de entrenamiento ( -t [nombreFichero] )." << endl;
		return EXIT_FAILURE;
	}
	if(Test == NULL)
	{
		cerr << "Falta fichero de test ( -T [nombreFichero] )." << endl;
		return EXIT_FAILURE;
	}

	Datos * pDatosTrain = mlp.leerDatos(train);
    Datos * pDatosTest = mlp.leerDatos(Test);

    //cout << "Patrones en train " << pDatosTrain->nNumPatrones << std::endl;
    //cout << "Patrones en test " << pDatosTest->nNumPatrones << std::endl;

    //cout << "Patron 1" << pDatosTrain->entradas[0][0] << pDatosTrain->entradas[0][1] << pDatosTrain->salidas[0][0] << pDatosTrain->salidas[0][1] << std::endl;


	mlp.dDecremento = F;
	mlp.dEta = eta;
	mlp.dMu = mu;
	mlp.dValidacion = validationRatio;
	mlp.bOnline = online;


	// Lectura de datos de entrenamiento y test: llamar a mlp.leerDatos(...)

	// Inicializar vector topología
	int *topologia = new int[hiddenLayers+2];
	int *tipo = new int[hiddenLayers+2];
	topologia[0] = pDatosTrain->nNumEntradas;
	for(int i=1; i<(hiddenLayers+2-1); i++)
		topologia[i] = hvalue;
	for(int i =0; i<hiddenLayers+2-1;i++)
	{
		tipo[i] = 0;
	}
	if(s == true){
		tipo[hiddenLayers+2-1] = 1;
	}
	else{
		tipo[hiddenLayers+2-1] = 0;

	}
	topologia[hiddenLayers+2-1] = pDatosTrain->nNumSalidas;


	// Inicializar red con vector de topología
	mlp.inicializar(hiddenLayers+2,topologia, tipo);


    // Semilla de los números aleatorios
    int semillas[] = {10,20,30,40,50};
    double *errores = new double[5];
    double *erroresTrain = new double[5];
    double *ccrs = new double[5];
    double *ccrsTrain = new double[5];
    for(int i=0; i<5; i++){
    	cout << "**********" << endl;
    	cout << "SEMILLA " << semillas[i] << endl;
    	cout << "**********" << endl;
		srand(semillas[i]);
		mlp.ejecutarAlgoritmo(pDatosTrain,pDatosTest,iteraciones,&(erroresTrain[i]),&(errores[i]),&(ccrsTrain[i]),&(ccrs[i]),fError);
		cout << "Finalizamos => CCR de test final: " << ccrs[i] << endl;
    }

    double mediaError = 0, desviacionTipicaError = 0;
    double mediaErrorTrain = 0, desviacionTipicaErrorTrain = 0;
    double mediaCCR = 0, desviacionTipicaCCR = 0;
    double mediaCCRTrain = 0, desviacionTipicaCCRTrain = 0;
    double sumaTrain = 0.0 , sumaTest = 0.0 , sumaCuadradoTrain = 0.0, sumaCuadradoTest = 0.0;
    double sumaCCRTrain = 0.0, sumaCCRTest = 0.0, sumaCuadradoCCRTrain = 0.0, sumaCuadradoCCRTest = 0.0;

    // Calcular medias y desviaciones típicas de entrenamiento y test
    for(int i=0; i<5; i++)
        {
        	sumaTrain += erroresTrain[i];
        	sumaTest += errores[i];
        	sumaCCRTrain += ccrsTrain[i];
        	sumaCCRTest += ccrs[i];
        	sumaCuadradoTrain += pow(erroresTrain[i],2);
        	sumaCuadradoTest += pow(errores[i],2);
        	sumaCuadradoCCRTrain += pow(ccrsTrain[i],2);
        	sumaCuadradoCCRTest += pow(ccrs[i],2);
        }

        mediaErrorTrain = sumaTrain / 5.0;
        mediaError = sumaTest / 5.0;
        desviacionTipicaErrorTrain = sqrt( (sumaCuadradoTrain/5.0)- pow(mediaErrorTrain,2) );
        desviacionTipicaError = sqrt( (sumaCuadradoTest/5.0)- pow(mediaError,2) );

        mediaCCRTrain = sumaCCRTrain / 5.0;
        mediaCCR = sumaCCRTest / 5.0;
        desviacionTipicaCCRTrain = sqrt( (sumaCuadradoCCRTrain/5.0) - pow(mediaCCRTrain,2) );
        desviacionTipicaCCR = sqrt( (sumaCuadradoCCRTest/5.0) - pow(mediaCCR,2));

    cout << "HEMOS TERMINADO TODAS LAS SEMILLAS" << endl;

	cout << "INFORME FINAL" << endl;
	cout << "*************" << endl;
    cout << "Error de entrenamiento (Media +- DT): " << mediaErrorTrain << " +- " << desviacionTipicaErrorTrain << endl;
    cout << "Error de test (Media +- DT): " << mediaError << " +- " << desviacionTipicaError << endl;
    cout << "CCR de entrenamiento (Media +- DT): " << mediaCCRTrain << " +- " << desviacionTipicaCCRTrain << endl;
    cout << "CCR de test (Media +- DT): " << mediaCCR << " +- " << desviacionTipicaCCR << endl;
	return EXIT_SUCCESS;
}

