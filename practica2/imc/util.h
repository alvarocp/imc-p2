/*
 * util.h
 *
 *  Created on: 06/03/2015
 *      Author: pedroa
 */

#ifndef UTIL_H_
#define UTIL_H_

namespace util{
int * vectorAleatoriosEnterosSinRepeticion(int minimo, int maximo, int cuantos);

static double aleatorio(double min, double max){
	return min + (rand() / (double) RAND_MAX * (max - min));

}



}



#endif /* UTIL_H_ */
