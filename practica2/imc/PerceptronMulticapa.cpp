/*********************************************************************
 * File  : PerceptronMulticapa.cpp
 * Date  : 2017
 *********************************************************************/

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <cstdlib>  // Para establecer la semilla srand() y generar números aleatorios rand()
#include <limits>
#include <cmath>

#include "PerceptronMulticapa.h"
#include "util.h"

using namespace imc;
using namespace std;
using namespace util;


// ------------------------------
// CONSTRUCTOR: Dar valor por defecto a todos los parámetros (dEta, dMu, dValidacion y dDecremento)
PerceptronMulticapa::PerceptronMulticapa(){
	nNumCapas = 3;
	pCapas = new Capa [nNumCapas];
	dEta = 0.7;
	dMu = 1;
	dDecremento = 1.0;
	dValidacion = 0.0;
	bOnline = false;
	nNumPatronesTrain = 1;
}

// ------------------------------
// Reservar memoria para las estructuras de datos
// nl tiene el numero de capas y npl es un vector que contiene el número de neuronas por cada una de las capas
// tipo contiene el tipo de cada capa (0 => sigmoide, 1 => softmax)
// Rellenar vector Capa* pCapas
int PerceptronMulticapa::inicializar(int nl, int npl[], int tipo[]) {

	nNumCapas = nl;
		pCapas = new Capa[nNumCapas];

		//Relleno la capa de entrada
		pCapas[0].nNumNeuronas = npl[0]; //Número de neuronas en capa de entrada
		pCapas[0].pNeuronas = new Neurona [pCapas[0].nNumNeuronas]; //Reservo neuronas en capa de entrada
		pCapas[0].tipo = tipo[0];

		for(int i=0; i < pCapas[0].nNumNeuronas; i++) //Relleno neuronas en capa de entrada (no hay pesos para la capa de entrada)
		{
			pCapas[0].pNeuronas[i].w = NULL;
			pCapas[0].pNeuronas[i].deltaW = NULL;
			pCapas[0].pNeuronas[i].ultimoDeltaW = NULL;
			pCapas[0].pNeuronas[i].wCopia = NULL;
		}

		for(int i=1; i < nNumCapas; i++)
		{
			pCapas[i].nNumNeuronas = npl[i]; //Número de neuronas
			pCapas[i].pNeuronas = new Neurona [npl[i]]; //Reservo neuronas
			pCapas[i].tipo = tipo[i];
			for(int j=0; j < pCapas[i].nNumNeuronas; j++) //Relleno neuronas
			{
				pCapas[i].pNeuronas[j].w = new double [pCapas[i-1].nNumNeuronas + 1];
				pCapas[i].pNeuronas[j].deltaW = new double [pCapas[i-1].nNumNeuronas + 1];
				pCapas[i].pNeuronas[j].ultimoDeltaW = new double [pCapas[i-1].nNumNeuronas + 1];
				pCapas[i].pNeuronas[j].wCopia = new double [pCapas[i-1].nNumNeuronas + 1];
			}
		}

		return 1;

	return 1;
}


// ------------------------------
// DESTRUCTOR: liberar memoria
PerceptronMulticapa::~PerceptronMulticapa() {
	liberarMemoria();
}


// ------------------------------
// Liberar memoria para las estructuras de datos
void PerceptronMulticapa::liberarMemoria() {
	for(int i=1; i<nNumCapas; i++)
		{
			for(int j=0; j<pCapas[i].nNumNeuronas; j++)
			{
				delete [] pCapas[i].pNeuronas[j].w;
				delete [] pCapas[i].pNeuronas[j].deltaW;
				delete [] pCapas[i].pNeuronas[j].ultimoDeltaW;
				delete [] pCapas[i].pNeuronas[j].wCopia;

			}
			delete [] pCapas[i].pNeuronas;
			pCapas[i].pNeuronas = NULL;
		}
		delete [] pCapas;
		pCapas = NULL;
}

// ------------------------------
// Rellenar todos los pesos (w) aleatoriamente entre -1 y 1
void PerceptronMulticapa::pesosAleatorios() {
	for(int i = 1; i<nNumCapas; i++)
		{
			for(int j=0; j<pCapas[i].nNumNeuronas; j++)
			{
				for(int k = 0; k< pCapas[i-1].nNumNeuronas +1; k++)
				{
					pCapas[i].pNeuronas[j].w[k] = aleatorio(-1,1);
					pCapas[i].pNeuronas[j].wCopia[k] = 0;
					pCapas[i].pNeuronas[j].deltaW[k] = 0;
					pCapas[i].pNeuronas[j].ultimoDeltaW[k] = 0;
				}
			}
		}
}

// ------------------------------
// Alimentar las neuronas de entrada de la red con un patrón pasado como argumento
void PerceptronMulticapa::alimentarEntradas(double* input) {
	for(int i = 0; i<pCapas[0].nNumNeuronas; i++)
	{
		pCapas[0].pNeuronas[i].x = input[i];
	}
}

// ------------------------------
// Recoger los valores predichos por la red (out de la capa de salida) y almacenarlos en el vector pasado como argumento
void PerceptronMulticapa::recogerSalidas(double* output){
	for(int i=0; i<pCapas[nNumCapas-1].nNumNeuronas; i++)
	{
		output[i] = pCapas[nNumCapas-1].pNeuronas[i].x;
	}
}

// ------------------------------
// Hacer una copia de todos los pesos (copiar w en copiaW)
void PerceptronMulticapa::copiarPesos() {
	for(int i=1; i<nNumCapas; i++)
		{
			for(int j=0; j<pCapas[i].nNumNeuronas;j++)
			{
				for(int k=0; k<pCapas[i-1].nNumNeuronas + 1; k++)
				{
					pCapas[i].pNeuronas[j].wCopia[k] = pCapas[i].pNeuronas[j].w[k];
				}
			}
		}
}

// ------------------------------
// Restaurar una copia de todos los pesos (copiar copiaW en w)
void PerceptronMulticapa::restaurarPesos() {
	for(int i=1; i<nNumCapas; i++)
		{
			for(int j=0; j<pCapas[i].nNumNeuronas;j++)
			{
				for(int k=0; k<pCapas[i-1].nNumNeuronas + 1; k++)
				{
					pCapas[i].pNeuronas[j].w[k] = pCapas[i].pNeuronas[j].wCopia[k];
				}
			}
		}
}

// ------------------------------
// Calcular y propagar las salidas de las neuronas, desde la primera capa hasta la última
void PerceptronMulticapa::propagarEntradas() {


	double aux, temp;
		temp = 0.0; //Para acumular los valores de salida para el posible calculo de la softmax

		for(int i=1; i < nNumCapas; i++) //Recorro capas
			for(int j=0; j < pCapas[i].nNumNeuronas; j++) //Recorro neuronas de la capa i
			{
				aux = 0.0; //Para acumular respecto a la capa anterior
				aux += pCapas[i].pNeuronas[j].w[0]; //Sumo el sesgo

				for(int k=0; k < pCapas[i-1].nNumNeuronas; k++) //Recorro los pesos respecto a la capa anterior
				{
					aux += pCapas[i].pNeuronas[j].w[k+1] * pCapas[i-1].pNeuronas[k].x;
				}
				if(pCapas[i].tipo == 0) //Si la capa es de tipo sigmoide...
				{
					pCapas[i].pNeuronas[j].x = (1 / ( 1 + exp(-aux) ) ); //Calculamos el valor de salida para cada neurona utilizando sigmoides
				}
				else if(pCapas[i].tipo == 1)
				{
					temp += pCapas[i].pNeuronas[j].x = exp(aux); //Calculo el valor de salida para cada neurona y lo acumulo en temp para utilizar la softmax
				}
			}

		if(pCapas[nNumCapas-1].tipo == 1) //Si la última capa es una capa softmax...
		{
			for(int j=0; j < pCapas[nNumCapas-1].nNumNeuronas; j++)
			{
				pCapas[nNumCapas-1].pNeuronas[j].x /= temp; //Dividimos por el valor temporal
			}
		}

}

// ------------------------------
// Calcular el error de salida del out de la capa de salida con respecto a un vector objetivo y devolverlo
// funcionError=1 => EntropiaCruzada // funcionError=0 => MSE
double PerceptronMulticapa::calcularErrorSalida(double* target, int funcionError) {
	double aux = 0.0; //Para acumular el error y dividirlo por el total de salidas

		if(funcionError == 0) //Si la función de error es el MSE...
		{
			for(int i=0; i < pCapas[nNumCapas-1].nNumNeuronas; i++) //Recorremos las salidas
			{
				aux += pow(target[i] - pCapas[nNumCapas-1].pNeuronas[i].x , 2); //Error cuadrático
			}
		}
		else if(funcionError == 1) //Si la funcion de error es la Entropía Cruzada...
		{
			for(int i=0; i < pCapas[nNumCapas-1].nNumNeuronas; i++) //Recorremos las salidas
			{
				aux -= target[i]*log(pCapas[nNumCapas-1].pNeuronas[i].x); //Entropía cruzada
			}
		}

		aux /= pCapas[nNumCapas-1].nNumNeuronas; //Dividimos por el número de salidas
		return aux;
}


// ------------------------------
// Retropropagar el error de salida con respecto a un vector pasado como argumento, desde la última capa hasta la primera
// funcionError=1 => EntropiaCruzada // funcionError=0 => MSE
void PerceptronMulticapa::retropropagarError(double* objetivo, int funcionError) {

		double out = 0.0, out2 = 0.0;
		double aux = 0.0;

		//Capa de salida -2(d - o ) o (1 - o)
		for(int i = 0; i<pCapas[nNumCapas-1].nNumNeuronas; i++)//Neuronas de la capa de salida
		{
			out = pCapas[nNumCapas-1].pNeuronas[i].x;

			if(pCapas[nNumCapas-1].tipo == 0) //Función sigmoide
			{
				if(funcionError == 0)//MSE
					pCapas[nNumCapas -1].pNeuronas[i].dX =   (objetivo[i] - out) * out * (1 - out);
				else ///Entropía
					pCapas[nNumCapas -1].pNeuronas[i].dX =   (objetivo[i] / out) * out * (1 - out);
			}
			else{ //Función softmax
				if(funcionError == 0)//MSE
				{
					for(int j=0; j < pCapas[nNumCapas-1].nNumNeuronas; j++)
						{
							out2 = pCapas[nNumCapas-1].pNeuronas[j].x; //Tomo la salida de la otra neurona
							aux += (objetivo[j]-out2)*out*( (j == i) - out2);
						}
				}
				else //Entropía Cruzada
					{
						for(int j=0; j < pCapas[nNumCapas-1].nNumNeuronas; j++)
						{
							out2 = pCapas[nNumCapas-1].pNeuronas[j].x;
							if(out2 != 0)
								aux += (objetivo[j]/out2)*out*( (j == i) - out2);
						}
					}
					pCapas[nNumCapas-1].pNeuronas[i].dX = -aux; //Almacenamos la derivada
			}

		}

		//Capas ocultas
		for(int i = (nNumCapas - 2); i > 0; i--)
		{
			for(int j = 0; j < pCapas[i].nNumNeuronas; j++)
			{
				aux = 0.0;
				out = pCapas[i].pNeuronas[j].x;
				for(int k = 0; k<pCapas[i+1].nNumNeuronas; k++)
				{
					//El peso 0 siempre corresponde con el sesgo; por eso j+1
					aux += pCapas[i+1].pNeuronas[k].w[j+1] * pCapas[i +1].pNeuronas[k].dX;
				}
				pCapas[i].pNeuronas[j].dX = aux * out * (1 - out);
			}
		}

}

// ------------------------------
// Acumular los cambios producidos por un patrón en deltaW
void PerceptronMulticapa::acumularCambio() {
	for(int i=1; i<nNumCapas; i++)
		{
			for(int j=0; j<pCapas[i].nNumNeuronas; j++)
			{
				pCapas[i].pNeuronas[j].deltaW[0] += pCapas[i].pNeuronas[j].dX;
				for(int k=1; k<pCapas[i -1].nNumNeuronas +1; k++)
				{
					pCapas[i].pNeuronas[j].deltaW[k] += pCapas[i].pNeuronas[j].dX * pCapas[i - 1].pNeuronas[k-1].x;
				}
			}
		}
}

// ------------------------------
// Actualizar los pesos de la red, desde la primera capa hasta la última
void PerceptronMulticapa::ajustarPesos() {
	for(int i = 1; i< nNumCapas; i++)
			{
				double newEta = pow(dDecremento, -1*(nNumCapas -1 -i)) * dEta;
				//cout << i << " newEta: " << newEta << std::endl;
				for(int j = 0; j<pCapas[i].nNumNeuronas; j++)
				{
					for(int k = 0; k<pCapas[i - 1].nNumNeuronas +1 ; k++)
					{
						if(bOnline)
							pCapas[i].pNeuronas[j].w[k] = pCapas[i].pNeuronas[j].w[k] - (newEta * pCapas[i].pNeuronas[j].deltaW[k]) - (dMu* newEta *pCapas[i].pNeuronas[j].ultimoDeltaW[k]);
						else
							pCapas[i].pNeuronas[j].w[k] = pCapas[i].pNeuronas[j].w[k] - ((newEta * pCapas[i].pNeuronas[j].deltaW[k]) - (dMu* newEta *pCapas[i].pNeuronas[j].ultimoDeltaW[k])/nNumPatronesTrain);

						//pCapas[i].pNeuronas[j].ultimoDeltaW[k] = pCapas[i].pNeuronas[j].deltaW[k];
						//pCapas[i].pNeuronas[j].deltaW[k] = 0.0;
					}
				}
			}
}

// ------------------------------
// Imprimir la red, es decir, todas las matrices de pesos
void PerceptronMulticapa::imprimirRed() {
	for(int i = 1; i<nNumCapas; i++)
		{
			for(int j = 0 ; j<pCapas[i].nNumNeuronas; j++)
			{
				for (int k = 0; k<pCapas[i - 1].nNumNeuronas +1; k++)
				{
					cout << "Conexion " << j+1 << " " << k << " capa " << i << " : " << pCapas[i].pNeuronas[j].w[k] <<  std::endl;
				}
			}
		}
}

// ------------------------------
// Simular la red: propragar las entradas hacia delante, computar el error, retropropagar el error y ajustar los pesos
// entrada es el vector de entradas del patrón, objetivo es el vector de salidas deseadas del patrón.
// El paso de ajustar pesos solo deberá hacerse si el algoritmo es on-line
// Si no lo es, el ajuste de pesos hay que hacerlo en la función "entrenar"
// funcionError=1 => EntropiaCruzada // funcionError=0 => MSE
void PerceptronMulticapa::simularRed(double* entrada, double* objetivo, int funcionError) {

	if(bOnline)
	{
		for(int i=1; i < nNumCapas; i++)
		{
			for(int j=0; j < pCapas[i].nNumNeuronas; j++)
			{
				for(int k=0; k < pCapas[i-1].nNumNeuronas; k++)
				{
					pCapas[i].pNeuronas[j].ultimoDeltaW[k] = pCapas[i].pNeuronas[j].deltaW[k];
					pCapas[i].pNeuronas[j].deltaW[k] = 0.0;
				}
			}
		}
	}

	alimentarEntradas(entrada);
	propagarEntradas();
	retropropagarError(objetivo,funcionError);
	acumularCambio();

	if(bOnline)
		ajustarPesos();


}

// ------------------------------
// Leer una matriz de datos a partir de un nombre de fichero y devolverla
Datos* PerceptronMulticapa::leerDatos(const char *archivo) {
	Datos* d = new Datos;

		ifstream fichero;
		fichero.open(archivo, ios::in);

		if(fichero.is_open()){

			fichero >> d->nNumEntradas >> d->nNumSalidas >> d->nNumPatrones;

			d->entradas = new double * [d->nNumPatrones];
			d->salidas = new double * [d->nNumPatrones];

			for(int i = 0; i<d->nNumPatrones; i++)
			{
				d->entradas[i] = new double [d->nNumEntradas];
				d->salidas[i] = new double [d->nNumSalidas];

				for(int j =0; j< d->nNumEntradas; j++)
				{
					fichero >> d->entradas[i][j];
				}

				for(int j=0; j<d->nNumSalidas; j++)
				{
					fichero >> d->salidas[i][j];
				}

			}

			fichero.close();

			return d;

		}
		else{
			cout << "Unable to open file " << "\n";
		}

		return d;
}


// ------------------------------
// Entrenar la red para un determinado fichero de datos (pasar una vez por todos los patrones)
void PerceptronMulticapa::entrenar(Datos* pDatosTrain, int funcionError) {
	if(!bOnline)
			for(int i=1; i < nNumCapas; i++)
			{
				for(int j=0; j < pCapas[i].nNumNeuronas; j++)
				{
					for(int k=0; k < pCapas[i-1].nNumNeuronas; k++)
					{
						pCapas[i].pNeuronas[j].ultimoDeltaW[k] = pCapas[i].pNeuronas[j].deltaW[k];
						pCapas[i].pNeuronas[j].deltaW[k] = 0.0;
					}
				}
			}

		for(int i=0; i<pDatosTrain->nNumPatrones; i++)
		{
			simularRed(pDatosTrain->entradas[i], pDatosTrain->salidas[i], funcionError);
		}
		if(!bOnline)
			ajustarPesos();
}

// ------------------------------
// Probar la red con un conjunto de datos y devolver el error cometido
// funcionError=1 => EntropiaCruzada // funcionError=0 => MSE
double PerceptronMulticapa::test(Datos* pDatosTest, int funcionError) {

	double error = 0.0;
	for(int i=0; i<pDatosTest->nNumPatrones; i++)
	{
		alimentarEntradas(pDatosTest->entradas[i]);
		propagarEntradas();
		error += calcularErrorSalida(pDatosTest->salidas[i], funcionError);
	}
	error /= pDatosTest->nNumPatrones;
	return error;
}


// ------------------------------
// Probar la red con un conjunto de datos y devolver el CCR
double PerceptronMulticapa::testClassification(Datos* pDatosTest) {

	double ccr = 0;

		for(int i=0; i < pDatosTest->nNumPatrones; i++)
		{
			double mayor = 0;
			int indice = 0;
			double * output = new double [pDatosTest->nNumSalidas];
			//Cargamos las entradas y propagamos el valor
			alimentarEntradas(pDatosTest->entradas[i]);
			propagarEntradas();
			recogerSalidas(output);
			for(int j=0; j<pDatosTest->nNumSalidas; j++)
			{
				if(output[j] > mayor)
				{
					mayor = output[j];
					indice = j;
				}
			}
			ccr += (pDatosTest->salidas[i][indice] == 1);
			delete[] output;
		}

		ccr /= pDatosTest->nNumPatrones;
		ccr *= 100;
		return ccr;
}

// ------------------------------
// Ejecutar el algoritmo de entrenamiento durante un número de iteraciones, utilizando pDatosTrain
// Una vez terminado, probar como funciona la red en pDatosTest
// Tanto el error MSE de entrenamiento como el error MSE de test debe calcularse y almacenarse en errorTrain y errorTest
// funcionError=1 => EntropiaCruzada // funcionError=0 => MSE
void PerceptronMulticapa::ejecutarAlgoritmo(Datos * pDatosTrain, Datos * pDatosTest, int maxiter, double *errorTrain, double *errorTest, double *ccrTrain, double *ccrTest, int funcionError)
{
	int countTrain = 0;

	// Inicialización de pesos
	pesosAleatorios();

	double minTrainError = 0;
	int numSinMejorar = 0;
	double testError = 0;
	nNumPatronesTrain = pDatosTrain->nNumPatrones;

	double validationError = 0.0;

	// Generar datos de validación
	if(dValidacion > 0 && dValidacion < 1){

	}

	// Aprendizaje del algoritmo
	do {

		entrenar(pDatosTrain,funcionError);
		double trainError = test(pDatosTrain,funcionError);
		if(countTrain==0 || fabs(trainError - minTrainError) > 0.00001){
			minTrainError = trainError;
			copiarPesos();
			numSinMejorar = 0;
		}
		else{
			numSinMejorar++;
		}

		if(numSinMejorar==50)
			countTrain = maxiter;

		testError = test(pDatosTest,funcionError);
		countTrain++;

		// Comprobar condiciones de parada de validación y forzar

		cout << "Iteración " << countTrain << "\t Error de entrenamiento: " << trainError << "\t Error de test: " << testError << "\t Error de validacion: " << validationError << endl;

	} while ( countTrain<maxiter );

		restaurarPesos();

	cout << "PESOS DE LA RED" << endl;
	cout << "===============" << endl;
	imprimirRed();

	cout << "Salida Esperada Vs Salida Obtenida (test)" << endl;
	cout << "=========================================" << endl;
	for(int i=0; i<pDatosTest->nNumPatrones; i++)
	{
		double* prediccion = new double[pDatosTest->nNumSalidas];

		// Cargamos las entradas y propagamos el valor
		alimentarEntradas(pDatosTest->entradas[i]);
		propagarEntradas();
		recogerSalidas(prediccion);
		for(int j=0; j<pDatosTest->nNumSalidas; j++)
			cout << pDatosTest->salidas[i][j] << " -- " << prediccion[j] << " \\\\ " ;
		cout << endl;
		delete[] prediccion;

	}

	*errorTest=test(pDatosTest,funcionError);;
	*errorTrain=minTrainError;
	*ccrTest = testClassification(pDatosTest);
	*ccrTrain = testClassification(pDatosTrain);

}

